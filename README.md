Get Office365 IPs, various scripts
Bash:
```bash
curl "https://endpoints.office.com/changes/worldwide/0000000000?Format=JSON&clientrequestid=`uuidgen`" | jq '.[]|.add.ips|.[]?' | grep '\.'
```


    Perl:
        getIPs.pl
        
        
    JAVA:
     getIPs_Office365.java
    
     To build:
        javac getIPs_Office365.java
     To run:
        java getIPs_Office365
        
