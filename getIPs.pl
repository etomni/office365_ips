#!/usr/bin/env perl
use strict;
use warnings;
use Data::GUID;
use LWP::UserAgent;
use JSON::Parse 'parse_json';

my $guid = Data::GUID->new;
my $clientId = $guid->as_string;

my $userAgent = new LWP::UserAgent;
$userAgent->timeout(30);

my $url = "https://endpoints.office.com/changes/worldwide/0000000000?Format=JSON&clientrequestid=$clientId";
my $request = new HTTP::Request('GET', $url);
my $response = $userAgent->request($request);

if ($response->is_success){
    my $content = $response->content();

    my $data = parse_json($content);

    foreach my $object ( @{ $data } ){
        foreach my $ip ( @{ $object->{add}->{ips}} ){
            print "$ip\n";  
        }
    }
}
else {
    print STDERR $response->status_line, "\n";
}
